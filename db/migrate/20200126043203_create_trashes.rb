class CreateTrashes < ActiveRecord::Migration[6.0]
  def change
    create_table :trashes do |t|
      t.decimal :latitude
      t.decimal :longitude
      t.time :start_time
      t.time :end_time
      t.string :address
      t.integer :fill_level

      t.timestamps
    end
  end
end
