# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# 0(1)
user = User.new(
  first_name: 'David',
  last_name: 'Hectarino',
  email: 'email@example.com',
  password: ''
)
user.password = 'mUc3m00RsqyRe'
user.password_confirmation = 'mUc3m00RsqyRe'
user.save

# Create Trashs

coords = [
  [-31.3954999, -64.2543264],
  [-31.39563639211989, -64.25348338277988],
  [-31.39618587244061, -64.25427731664355],
  [-31.392669142778075, -64.25674494893363],
  [-31.39283399242985, -64.25202426105251],
  [-31.387906694713088, -64.2523890414981],
  [-31.3867893161301, -64.25283965261664],
  [-31.3873022128802, -64.25253924521249],
  [-31.38755866019922, -64.25221738013026],
  [-31.39285230901318, -64.23644599111705],
  [-31.41196483961305, -64.25139895378084],
  [-31.4673843942321, -64.26632494885794],
  [-31.449035550881618, -64.08459187818453],
  [-31.34855380141513, -64.18096415377083],
  [-31.365221424747624, -64.26696726045607],
  [-31.41572709908823, -64.1120396782017],
  [-31.401150930679293, -64.05836398042837]
]

# 0(n)
coords.each { |n|
  Trash.create(
    latitude: n[0],
    longitude: n[1],
    start_time: '12:00',
    end_time: '22:00',
    address: 'Colon 6200, Cordoba',
    fill_level: rand(1..100)
  )
}

ratings = [['Very sad', 1], ['Normal', 3], ['Amazing', 7], ['Perfect, i can eat in it', 10]]

# O(10)
10.times { |n|
  rating = ratings[rand(0..3)]
  Review.create(
    comment: rating[0],
    rating: rating[1],
    trash_id: rand(1..(coords.length - 1)),
    user_id: user.id
  )
}



# Create Reviews

