Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  post '/login', to: 'authentication#login'
  get '/user', to: 'user#get_current_user'
  post '/user', to: 'user#create'
  get '/search', to: 'trash#search_containers'
  get '/details/:id_trash', to: 'trash#get_details'
  post '/review/:id_trash', to: 'trash#create_review'
  put '/review/:id_review', to: 'trash#update_review'
  delete '/review/:id_review', to: 'trash#delete_review'
end
