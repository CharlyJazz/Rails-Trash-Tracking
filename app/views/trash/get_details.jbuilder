json.trash @api_trash
json.rating @api_trash.reviews.average(:rating).round
json.reviews do
  json.array! @api_trash.reviews do |n|
    json.id n.id
    json.comment n.comment
    json.rating n.rating
    json.created_at n.created_at
    json.updated_at n.updated_at
    json.user n.user
  end
end
