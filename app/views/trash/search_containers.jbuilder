json.array! @containers do |n|
  json.id n.id
  json.latitude n.latitude
  json.longitude n.longitude
  json.address n.address
  json.fill_level n.fill_level
  json.start_time n.start_time
  json.end_time n.end_time
  json.rating n.reviews.length > 0 ? n.reviews.average(:rating).round : 0
  json.reviews do
    json.array! n.reviews do |n|
      json.id n.id
      json.comment n.comment
      json.rating n.rating
      json.created_at n.created_at
      json.updated_at n.updated_at
      json.user n.user
    end
  end
end


