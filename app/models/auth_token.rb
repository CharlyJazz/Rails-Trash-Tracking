class AuthToken
  def self.key
    Rails.application.secrets.secret_key_base
  end

  def self.encode(payload, exp = 24.hours.from_now)
    payload[:exp] = exp.to_i
    JWT.encode(payload, key)
  end

  def self.decode(token)
    decoded = JWT.decode(token, key)[0]
    HashWithIndifferentAccess.new decoded
  end
end
