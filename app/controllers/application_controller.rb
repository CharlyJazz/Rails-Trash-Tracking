class ApplicationController < ActionController::API
    def not_found
        render json: { error: 'Not found' }
    end

    def authorize_request
        header = request.headers['Authorization']
        header = header.split(' ').last if header
        begin
          @decoded = AuthToken.decode(header)
          @current_user = User.find(@decoded[:user_id])
        rescue ActiveRecord::RecordNotFound => e
          render json: { errors: e.message }, status: :unauthorized
        rescue JWT::DecodeError => e
          render json: { errors: e.message }, status: :unauthorized
        end
    end

    def authorize_request_optional
      header = request.headers['Authorization']
      header = header.split(' ').last if header
      begin
        @decoded = AuthToken.decode(header)
        @current_user = User.find(@decoded[:user_id])
      rescue ActiveRecord::RecordNotFound => e
        @current_user = nil
      rescue JWT::DecodeError => e
        @current_user = nil
      end
  end
end
