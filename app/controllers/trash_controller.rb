class TrashController < ApplicationController
  before_action :set_trash, only: [:get_details, :create_review]
  before_action :authorize_request, only: [:create_review, :update_review, :delete_review]
  before_action :set_review, only: [:update_review, :delete_review]

  #  Buscar contenedores cerca de mi
  def search_containers
    @containers = Trash.all
  end

  # Retrieve details of the container
  def get_details
  end

  # Crear review de un container
  def create_review
    @review = Review.new(reviews_params.merge({ :user => @current_user, :trash => @api_trash}))
    if @review.save
      render json: @review, status: :created
    else
      render json: @review.errors, status: :unprocessable_entity
    end
  end

  # Actualizar review
  def update_review
    return render json: @api_review.errors, status: :unauthorized unless @current_user.is_owner(@api_review)
    if @api_review.update(reviews_params)
      render json: @api_review
    else
      render json: @api_review.errors, status: :unprocessable_entity
    end
  end

  # Eliminar review
  def delete_review
    return render json: @api_review.errors, status: :unauthorized unless @current_user.is_owner(@api_review)
    @api_review.destroy
  end

  private

  def set_review
    @api_review = Review.find(params[:id_review])
  end

  def set_trash
    @api_trash = Trash.find(params[:id_trash])
  end

  def reviews_params
    params.permit(
      :comment,
      :rating
    )
  end
end
