class AuthenticationController < ApplicationController
  # Crear token
  def login
    @user = User.find_by_email(params[:email])
    if !@user.nil? && @user.email && @user&.authenticate(params[:password])
      token = AuthToken.encode(user_id: @user.id)
      time = Time.now + 24.hours.to_i
      render json: {
        token: token,
        exp: time.strftime("%m-%d-%Y %H:%M"),
        email: @user.email,
        id: @user.id,
      }, status: :ok
    else
      render json: {
        message: 'Credentials wrongs',
        state: 401
      }, status: :unauthorized
    end
  end
end
