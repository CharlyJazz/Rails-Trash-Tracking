class UserController < ApplicationController
  before_action :authorize_request, only: [:get_current_user]

  #  Register user
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # Datos del usuario y de los reviews que ha hecho
  def get_current_user
    render json: @current_user, except: [:password_digest]
  end

  private

  def user_params
    params.permit(
      :first_name,
      :last_name,
      :email,
      :password,
      :password_digest
    )
  end
end
