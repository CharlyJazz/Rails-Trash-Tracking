import React from "react";
import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home";
import { AppContext } from "./state";
import { Router } from "@reach/router";

const SwitchRootView = () => {
  const [state] = React.useContext(AppContext);
  return !state.user ? (
    <Router>
      <Login path="/" />
      <Register path="/signup" />
    </Router>
  ) : (
    <Router>
      <Home path="/" />
    </Router>
  );
};
export default SwitchRootView;
