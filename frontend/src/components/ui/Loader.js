import React from "react";

export const Loader = () => {
  return (
    <div
      style={{
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
        height: "100vh",
        backgroundColor: "#009688"
      }}
    >
      <div className="loader">Loading...</div>
    </div>
  );
};
