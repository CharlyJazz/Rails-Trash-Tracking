import React, { useEffect, useState, useContext } from "react";
import GoogleMapReact from "google-map-react";
import { KEY, URL_BASE } from "../../constants";
import { Loader } from "../ui/Loader";
import { usePosition } from "use-position";
import useSWR, { mutate } from "swr";
import { TrashIcon } from "../ui/TrashIcon";
import Modal from "react-modal";
import { StartIcon } from "../ui/StartIcon";
import { AppContext } from "../../state";

const customStyles = {
  overlay: {
    backgroundColor: "rgba(113, 113, 113, 0.75)"
  },
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    boxShadow: "rgb(113, 113, 113) 0px 0px 10px 3px"
  }
};

// Mister @Jacob's Anwser
var percentColors = [
  { pct: 0.0, color: { r: 0xff, g: 0x00, b: 0 } },
  { pct: 0.5, color: { r: 0xff, g: 0xff, b: 0 } },
  { pct: 1.0, color: { r: 0x00, g: 0xff, b: 0 } }
];

var getColorForPercentage = function(pct) {
  for (var i = 1; i < percentColors.length - 1; i++) {
    if (pct < percentColors[i].pct) {
      break;
    }
  }
  var lower = percentColors[i - 1];
  var upper = percentColors[i];
  var range = upper.pct - lower.pct;
  var rangePct = (pct - lower.pct) / range;
  var pctLower = 1 - rangePct;
  var pctUpper = rangePct;
  var color = {
    r: Math.floor(lower.color.g * pctLower + upper.color.g * pctUpper),
    g: Math.floor(lower.color.r * pctLower + upper.color.r * pctUpper),
    b: Math.floor(lower.color.b * pctLower + upper.color.b * pctUpper)
  };
  return "rgb(" + [color.r, color.g, color.b].join(",") + ")";
  // or output as hex if preferred
};
const Marker = ({ onClick, data }) => {
  const fill = parseInt(data.fill_level);
  return (
    <div
      className="MarkerContainer"
      onClick={onClick}
      style={{ width: fill < 99 ? 58 : 66 }}
    >
      <TrashIcon />
      <span
        style={{
          color: getColorForPercentage(fill / 100)
        }}
      >{`${data.fill_level}%`}</span>
    </div>
  );
};

const Me = () => {
  return (
    <div className="Me">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="40"
        height="40"
        viewBox="0 0 24 24"
      >
        <path
          d="M12 2c-4.97 0-9 4.03-9 9 0 4.17 2.84 7.67 6.69 8.69L12 22l2.31-2.31C18.16 18.67 21 15.17 21 11c0-4.97-4.03-9-9-9zm0 2c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.3c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z"
          fill="#009688"
        />
        <path fill="none" d="M0 0h24v24H0z" />
      </svg>
    </div>
  );
};

const Home = () => {
  const [user, dispatch] = useContext(AppContext);
  const logout = () => {
    localStorage.removeItem("user");
    dispatch({
      type: "LOGOUT"
    });
  };
  const { latitude, longitude } = usePosition();
  const [modalIsOpen, setIsOpen] = useState(false);
  const [containerFocus, setContainerFocus] = useState(null);
  const [reviewMode, setReviewMode] = useState(false);
  const [comment, setComment] = useState("");
  const [stars, setStars] = useState(1);
  const { data } = useSWR(URL_BASE + "/search", url =>
    fetch(url).then(_ => _.json())
  );
  useEffect(() => {
    Modal.setAppElement("#root");
  }, []);
  const onClickMarker = container => {
    setIsOpen(true);
    setContainerFocus(container);
  };
  const onCloseModal = () => {
    setIsOpen(false);
    setReviewMode(false);
    setStars(1);
    setComment("");
  };

  const createReview = async () => {
    const review = await fetch(URL_BASE + "/review/" + containerFocus.id, {
      method: "POST",
      headers: {
        Authorization: localStorage.getItem("jwt"),
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ rating: parseInt(stars), comment: comment })
    })
      .then(res => res.json())
      .catch(() => {});
    if (review.id) {
      const newContainerFocus = { ...containerFocus };
      const index = data.findIndex(n => n.id === containerFocus.id);
      const left = data.slice(0, index);
      const right = data.slice(index + 1);
      const new_data = [...left, newContainerFocus, ...right];
      mutate(URL_BASE + "/search", new_data, false);
      newContainerFocus.reviews.push({
        ...review,
        user: {
          id: user.id,
          first_name: "Yo recientemente",
          last_name: ""
        }
      });
      onCloseModal();
    }
  };
  const deleteReview = async id_review => {
    await fetch(URL_BASE + "/review/" + id_review, {
      method: "DELETE",
      headers: {
        Authorization: localStorage.getItem("jwt")
      }
    }).catch(res => console.log(res));
    const index = data.findIndex(n => n.id === containerFocus.id);
    const newContainerFocus = { ...containerFocus };
    const newReviews = newContainerFocus.reviews.filter(
      n => n.id !== id_review
    );
    newContainerFocus.reviews = newReviews;
    const left = data.slice(0, index);
    const right = data.slice(index + 1);
    const new_data = [...left, newContainerFocus, ...right];
    mutate(URL_BASE + "/search", new_data, false);
    onCloseModal();
  };
  if (latitude && longitude) {
    return (
      <div style={{ height: "100vh", width: "100%" }}>
        <button className="Button logout" onClick={logout}>
          Salir
        </button>
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={onCloseModal}
          style={customStyles}
          contentLabel="Trash Container"
        >
          {containerFocus ? (
            <div>
              <div className="bar-progress">
                <div
                  className="green"
                  data-width={`${containerFocus.fill_level}%`}
                  style={{ width: `${containerFocus.fill_level}%` }}
                />
              </div>
              <br />
              {reviewMode ? (
                <div className="form">
                  <div className="detail-item">
                    <div>
                      <p>
                        <strong>Agrega el puntaje y un comentario:</strong>
                      </p>
                    </div>
                  </div>
                  <div className="input-field">
                    <label htmlFor="comment">Comentario</label>
                    <input
                      id="comment"
                      name="comment"
                      placeholder="Bla bla bla"
                      required
                      value={comment}
                      onChange={event => setComment(event.target.value)}
                    />
                  </div>
                  <div className="input-field">
                    <label htmlFor="rating">Estrellas</label>
                    <select
                      id="rating"
                      name="rating"
                      placeholder="2 Estrellas"
                      required
                      value={stars}
                      onChange={event => setStars(event.target.value)}
                    >
                      {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(n => (
                        <option value={n} key={n}>
                          {n}
                        </option>
                      ))}
                    </select>
                  </div>
                  <div className="input-field">
                    <br />
                    <br />
                    <button
                      disabled={!comment}
                      onClick={createReview}
                      className="Button"
                    >
                      {comment ? "Enviar" : "Escribe el comentario"}
                    </button>
                    <br />
                    <br />
                  </div>
                </div>
              ) : (
                <>
                  <div className="detail-item">
                    <div>
                      <p>
                        <strong>Direccion:</strong>
                      </p>
                    </div>
                    <div>
                      <p> {containerFocus.address}</p>
                    </div>
                  </div>
                  <div className="detail-item">
                    <div>
                      <p>
                        <strong>Horario:</strong>
                      </p>
                    </div>
                    <div>
                      <p>
                        {` ${
                          new Date(containerFocus.start_time)
                            .toTimeString()
                            .split(" ")[0]
                        } - ${
                          new Date(containerFocus.end_time)
                            .toTimeString()
                            .split(" ")[0]
                        } `}
                      </p>
                    </div>
                  </div>
                  <div className="detail-item">
                    <div>
                      <p>
                        <strong>Total reviews:</strong>
                      </p>
                    </div>
                    <div>
                      <p>{containerFocus.reviews.length}</p>
                    </div>
                  </div>
                  <div className="review-wrapper">
                    {containerFocus.reviews.map(n => {
                      return (
                        <div className="review-item" key={n.id}>
                          <p>
                            <strong>
                              {n.user.first_name} {n.user.last_name}:
                            </strong>
                            {user.id === n.user.id ? (
                              <button
                                className="delete-button"
                                onClick={() => deleteReview(n.id)}
                              >
                                Eliminar
                              </button>
                            ) : null}
                            <br />
                            {n.comment}
                            <br />
                            {new Array(n.rating).fill(undefined).map((n, i) => (
                              <StartIcon key={i} />
                            ))}
                          </p>
                        </div>
                      );
                    })}
                  </div>
                  <br />
                </>
              )}
            </div>
          ) : null}
          <div className="detail-item">
            <div>
              <button
                className="Button"
                onClick={() => setReviewMode(!reviewMode)}
              >
                {reviewMode ? (
                  "Cancelar"
                ) : (
                  <>
                    Crear Review <StartIcon />
                  </>
                )}
              </button>
            </div>
            <div>
              <button className="Button" onClick={onCloseModal}>
                Salir
              </button>
            </div>
          </div>
        </Modal>
        <GoogleMapReact
          bootstrapURLKeys={{ key: KEY }}
          defaultCenter={{
            lat: latitude,
            lng: longitude
          }}
          defaultZoom={17}
          options={{
            maxZoom: 18,
            minZoom: 13
          }}
        >
          <Me
            {...{
              lat: latitude,
              lng: longitude
            }}
          />
          {data && Array.isArray(data) && data.length
            ? data.map(n => {
                return (
                  <Marker
                    {...{
                      key: n.id,
                      lat: Number(n.latitude),
                      lng: Number(n.longitude)
                    }}
                    data={n}
                    onClick={() => onClickMarker(n)}
                  />
                );
              })
            : null}
        </GoogleMapReact>
      </div>
    );
  } else {
    return <Loader />;
  }
};

export default Home;
