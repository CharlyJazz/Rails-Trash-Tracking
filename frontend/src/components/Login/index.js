import React from "react";
import { AppContext } from "../../state";
import { URL_BASE } from "../../constants";
import { Link } from "@reach/router";

const Login = () => {
  const [, dispatch] = React.useContext(AppContext);
  const isDev = process.env.NODE_ENV === "development";
  const [password, setPassword] = React.useState(isDev ? "123456" : "");
  const [email, setEmail] = React.useState(isDev ? "emailx@email.com" : "");

  const handleSubmit = event => {
    event.preventDefault();

    fetch(URL_BASE + "/login", {
      method: "POST",
      body: JSON.stringify({
        email,
        password
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(res => {
        if (res.email && res.id && res.token) {
          const userData = { email: res.email, id: res.id };
          localStorage.setItem("jwt", res.token);
          localStorage.setItem("user", JSON.stringify(userData));
          dispatch({
            type: "LOGIN_SUCCESS",
            user: userData,
            token: res.token
          });
        } else {
          alert("Datos incorrectos");
        }
      });
  };

  return (
    <div className="Login">
      <form className="Login__form" onSubmit={handleSubmit}>
        <h1>Acceder</h1>
        <div className="Login__form-group">
          <label>Email</label>
          <input
            type="email"
            required
            placeholder="Email Address"
            value={email}
            onChange={event => setEmail(event.target.value)}
          />
        </div>
        <div className="Login__form-group">
          <label>Password</label>
          <input
            type="password"
            required
            placeholder="Password"
            value={password}
            onChange={event => setPassword(event.target.value)}
          />
        </div>
        <button className="Login__form-button">Login</button>
        <br />
        <br />
        <Link to="/signup">Registrate</Link>
      </form>
    </div>
  );
};

export default Login;
