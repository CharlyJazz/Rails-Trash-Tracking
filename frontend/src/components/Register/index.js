import React from "react";
import { URL_BASE } from "../../constants";
import { Link, navigate } from "@reach/router";

const Register = () => {
  const isDev = process.env.NODE_ENV === "development";
  const [password, setPassword] = React.useState(isDev ? "123456" : "");
  const [email, setEmail] = React.useState(isDev ? "emailx@email.com" : "");
  const [nombre, setNombre] = React.useState(isDev ? "carlos" : "");
  const [apellido, setApellido] = React.useState(isDev ? "zambrano" : "");

  const handleSubmit = event => {
    event.preventDefault();

    fetch(URL_BASE + "/user", {
      method: "POST",
      body: JSON.stringify({
        first_name: nombre,
        last_name: apellido,
        email: email,
        password: password,
        password_diges: password
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(() => navigate("/"))
      .catch(() => alert("Intenta con otros datos"));
  };

  return (
    <div className="Login">
      <form className="Login__form" onSubmit={handleSubmit}>
        <h1>Registrarte</h1>
        <div className="Login__form-group">
          <label>Primer Nombre</label>
          <input
            required
            placeholder="Tu primer nombre"
            value={nombre}
            onChange={event => setNombre(event.target.value)}
          />
        </div>
        <div className="Login__form-group">
          <label>Apellido</label>
          <input
            required
            placeholder="Tu primer apellido"
            value={apellido}
            onChange={event => setApellido(event.target.value)}
          />
        </div>
        <div className="Login__form-group">
          <label>Email</label>
          <input
            type="email"
            required
            placeholder="Email Address"
            value={email}
            onChange={event => setEmail(event.target.value)}
          />
        </div>
        <div className="Login__form-group">
          <label>Password</label>
          <input
            type="password"
            required
            placeholder="Password"
            value={password}
            onChange={event => setPassword(event.target.value)}
          />
        </div>
        <button className="Login__form-button">Crear cuenta</button>
        <br />
        <br />
        <Link to="/">Acceder</Link>
      </form>
    </div>
  );
};

export default Register;
