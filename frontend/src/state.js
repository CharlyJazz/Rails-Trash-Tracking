import React from "react";

const initialState = {
  user: JSON.parse(localStorage.getItem("user")),
  token: localStorage.getItem("jwt")
};

const reducer = (state, action) => {
  switch (action.type) {
    case "LOGOUT":
      return { user: null, token: null };
    case "LOGIN_SUCCESS":
      return {
        ...state,
        user: action.user,
        token: action.token
      };
    default:
      throw new Error("Unexpected action");
  }
};

const AppContext = React.createContext();

const useApp = () => {
  const contextValue = React.useContext(AppContext);
  return contextValue;
};

const AppProvider = ({ children }) => {
  const contextValue = React.useReducer(reducer, initialState);
  return (
    <AppContext.Provider value={contextValue}>{children}</AppContext.Provider>
  );
};

export { AppProvider, AppContext, useApp };
