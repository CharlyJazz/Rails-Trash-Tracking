import React from "react";
import { AppProvider } from "./state";
import SwitchRootView from "./SwitchRootView";

function App() {
  return (
    <AppProvider>
      <SwitchRootView />
    </AppProvider>
  );
}

export default App;
